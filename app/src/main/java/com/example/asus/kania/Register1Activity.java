package com.example.asus.kania;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Register1Activity extends AppCompatActivity {
    Button daftar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register1);
        daftar = (Button)findViewById(R.id.daftar);
        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupwrong();
            }
        });
    }

    public void popupwrong(){
        final Dialog dialogwrong = new Dialog(Register1Activity.this);
        dialogwrong.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogwrong.setContentView(R.layout.popupregister);
        Button ok_button = (Button)dialogwrong.findViewById(R.id.ok_button);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Register1Activity.this, Register2Activity.class);
                startActivity(intent);
            }
        });
        ImageView close = (ImageView)dialogwrong.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogwrong.dismiss();
            }
        });


        dialogwrong.show();
    }
}
