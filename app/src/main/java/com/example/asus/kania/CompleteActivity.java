package com.example.asus.kania;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CompleteActivity extends AppCompatActivity {
    TextView kode;
    ImageView copy;
    Button video;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete);
        kode = (TextView)findViewById(R.id.kode);
        copy = (ImageView)findViewById(R.id.copy_icon);
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(
                        kode.getText().toString(), // What should I set for this "label"?
                        kode.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(CompleteActivity.this, "Kode Referensi Sudah di Salin", Toast.LENGTH_SHORT).show();
            }
        });
        video = (Button)findViewById(R.id.video);
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CompleteActivity.this, VideoActivity.class);
                startActivity(intent);
            }
        });
    }
}
