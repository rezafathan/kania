package com.example.asus.kania;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class TaplusMudaActivity extends AppCompatActivity {
    TextView bni_taplus_muda;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taplus_muda);
        bni_taplus_muda = (TextView)findViewById(R.id.taplus_muda_text);
        Spannable wordtoSpan = new SpannableString("BNI Taplus Muda merupakan produk tabungan yang diperuntukkan bagi anak muda Indonesia mulai dari usia 17 tahun sampai dengan 25 tahun.");
        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.orange)), 0, 15, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        bni_taplus_muda.setText(wordtoSpan);

        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaplusMudaActivity.super.onBackPressed();
            }
        });
    }
}
