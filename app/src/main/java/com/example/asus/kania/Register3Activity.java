package com.example.asus.kania;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;

public class Register3Activity extends AppCompatActivity {
    ImageView back;
    CardView card_taplus, card_taplus_muda;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register3);
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register3Activity.super.onBackPressed();
            }
        });
        card_taplus = (CardView)findViewById(R.id.card_taplus);
        card_taplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Register3Activity.this, TaplusActivity.class);
                startActivity(intent);
            }
        });
        card_taplus_muda = (CardView)findViewById(R.id.card_taplus_muda);
        card_taplus_muda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Register3Activity.this, TaplusMudaActivity.class);
                startActivity(intent);
            }
        });
    }
}
