package com.example.asus.kania;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class OTPActivity extends AppCompatActivity {
    Button button_1,button_2,button_3,button_4,button_5,button_6,button_7,button_8,button_9, button_0;
    ImageButton button_clear;
    ImageView round1,round2,round3,round4,round5,round6, back;
    EditText editotp;
    TextView timer;
    CountDownTimer countDownTimer;
    private long time = 60000; //1 Menit
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        button_1 = (Button)findViewById(R.id.button_1);
        button_2 = (Button)findViewById(R.id.button_2);
        button_3 = (Button)findViewById(R.id.button_3);
        button_4 = (Button)findViewById(R.id.button_4);
        button_5 = (Button)findViewById(R.id.button_5);
        button_6 = (Button)findViewById(R.id.button_6);
        button_7 = (Button)findViewById(R.id.button_7);
        button_8 = (Button)findViewById(R.id.button_8);
        button_9 = (Button)findViewById(R.id.button_9);
        button_clear = (ImageButton) findViewById(R.id.button_clear);
        button_0 = (Button)findViewById(R.id.button_0);

        round1 = (ImageView)findViewById(R.id.round_1);
        round2 = (ImageView)findViewById(R.id.round_2);
        round3 = (ImageView)findViewById(R.id.round_3);
        round4 = (ImageView)findViewById(R.id.round_4);
        round5 = (ImageView)findViewById(R.id.round_5);
        round6 = (ImageView)findViewById(R.id.round_6);

        button_click();

        editotp = (EditText)findViewById(R.id.editotp);
        editotp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editotp.getText().toString().length()==0){
                    round1.setImageResource(R.drawable.round_grey);
                    round2.setImageResource(R.drawable.round_grey);
                    round3.setImageResource(R.drawable.round_grey);
                    round4.setImageResource(R.drawable.round_grey);
                    round5.setImageResource(R.drawable.round_grey);
                    round6.setImageResource(R.drawable.round_grey);
                }
                else if(editotp.getText().toString().length()==1){
                    round1.setImageResource(R.drawable.round_orange);
                    round2.setImageResource(R.drawable.round_grey);
                    round3.setImageResource(R.drawable.round_grey);
                    round4.setImageResource(R.drawable.round_grey);
                    round5.setImageResource(R.drawable.round_grey);
                    round6.setImageResource(R.drawable.round_grey);
                }
                else if(editotp.getText().toString().length()==2){
                    round1.setImageResource(R.drawable.round_orange);
                    round2.setImageResource(R.drawable.round_orange);
                    round3.setImageResource(R.drawable.round_grey);
                    round4.setImageResource(R.drawable.round_grey);
                    round5.setImageResource(R.drawable.round_grey);
                    round6.setImageResource(R.drawable.round_grey);
                }
                else if(editotp.getText().toString().length()==3){
                    round1.setImageResource(R.drawable.round_orange);
                    round2.setImageResource(R.drawable.round_orange);
                    round3.setImageResource(R.drawable.round_orange);
                    round4.setImageResource(R.drawable.round_grey);
                    round5.setImageResource(R.drawable.round_grey);
                    round6.setImageResource(R.drawable.round_grey);
                }
                else if(editotp.getText().toString().length()==4){
                    round1.setImageResource(R.drawable.round_orange);
                    round2.setImageResource(R.drawable.round_orange);
                    round3.setImageResource(R.drawable.round_orange);
                    round4.setImageResource(R.drawable.round_orange);
                    round5.setImageResource(R.drawable.round_grey);
                    round6.setImageResource(R.drawable.round_grey);
                }
                else if(editotp.getText().toString().length()==5){
                    round1.setImageResource(R.drawable.round_orange);
                    round2.setImageResource(R.drawable.round_orange);
                    round3.setImageResource(R.drawable.round_orange);
                    round4.setImageResource(R.drawable.round_orange);
                    round5.setImageResource(R.drawable.round_orange);
                    round6.setImageResource(R.drawable.round_grey);
                }
                else if(editotp.getText().toString().length()==6){
                    round1.setImageResource(R.drawable.round_orange);
                    round2.setImageResource(R.drawable.round_orange);
                    round3.setImageResource(R.drawable.round_orange);
                    round4.setImageResource(R.drawable.round_orange);
                    round5.setImageResource(R.drawable.round_orange);
                    round6.setImageResource(R.drawable.round_orange);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OTPActivity.super.onBackPressed();
            }
        });

        timer = (TextView)findViewById(R.id.timer);
        start_timer();
    }
    public void start_timer(){
        countDownTimer = new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                time = millisUntilFinished;
                int minutes = (int)time/60000;
                int seconds = (int)time % 60000/1000;
                String timeleft;
                timeleft = "" +minutes;
                timeleft +=":";
                if (seconds < 10)timeleft += "0";
                timeleft += seconds;

                timer.setText(timeleft);
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }
    public void button_click(){
        button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editotp.setText(editotp.getText()+"1");
            }
        });
        button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editotp.setText(editotp.getText()+"2");
            }
        });
        button_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editotp.setText(editotp.getText()+"3");
            }
        });
        button_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editotp.setText(editotp.getText()+"4");
            }
        });
        button_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editotp.setText(editotp.getText()+"5");
            }
        });
        button_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editotp.setText(editotp.getText()+"6");
            }
        });
        button_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editotp.setText(editotp.getText()+"7");
            }
        });
        button_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editotp.setText(editotp.getText()+"8");
            }
        });
        button_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editotp.setText(editotp.getText()+"9");
            }
        });
        button_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editotp.setText(editotp.getText()+"0");
            }
        });
        button_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String delete = editotp.getText().toString();
                int input = delete.length();
                if (input > 0){
                    editotp.setText(delete.substring(0, input-1));
                }
            }
        });
    }
}
