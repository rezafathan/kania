package com.example.asus.kania;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class TaplusActivity extends AppCompatActivity {
    TextView bni_taplus;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taplus);
        bni_taplus = (TextView)findViewById(R.id.taplus_muda);
        Spannable wordtoSpan = new SpannableString("BNI Taplus memberikan kemudahan, kenyamanan layanan dan banyak keuntungan untuk berbagai aktivitas transaksi perbankan Anda");
        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.orange)), 0, 10, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        bni_taplus.setText(wordtoSpan);

        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaplusActivity.super.onBackPressed();
            }
        });
    }
}
