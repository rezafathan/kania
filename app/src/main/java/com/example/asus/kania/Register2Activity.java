package com.example.asus.kania;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;

public class Register2Activity extends AppCompatActivity {
    ImageView back;
    ScrollView scrollView;
    Button button_setuju;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);
        button_setuju = (Button)findViewById(R.id.button_Setuju);
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register2Activity.super.onBackPressed();
            }
        });
        scrollView = (ScrollView)findViewById(R.id.scroll_register);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (scrollView.getChildAt(0).getBottom()
                        <= (scrollView.getHeight() + scrollView.getScrollY())){
                    button_setuju.setBackground(getResources().getDrawable(R.drawable.button_rectange_orange));
                    button_setuju.setTextColor(getResources().getColor(R.color.white));
                    button_setuju.setEnabled(true);
                }
                else{
                    button_setuju.setBackground(getResources().getDrawable(R.drawable.button_rectangle_gray));
                    button_setuju.setTextColor(getResources().getColor(R.color.button_text_disable));
                    button_setuju.setEnabled(false);
                }
            }
        });

        button_setuju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Register2Activity.this, Register3Activity.class);
                startActivity(intent);
            }
        });

    }
}
